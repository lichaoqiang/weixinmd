﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WeiXin.Sdk.Common.Extensions
{
    /// <summary>
    /// <![CDATA[批量发送消息]]>
    /// </summary>
    public static class MassMessageClientExtentions
    {

        /// <summary>
        /// <![CDATA[根据指定标签，群发消息]]>
        /// </summary>
        /// <param name="oAuthClient"></param>
        /// <param name="access_token"></param>
        /// <param name="tagMessage"></param>
        /// <returns><![CDATA[{"errcode":0,"errmsg":"send job submission success","msg_id":34182,"msg_data_id":206227730}]]></returns>
        public static async Task<string> SendAll(this OAuth.IOAuthClient oAuthClient, string access_token, Domain.Tagging.TagMessageBase tagMessage)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={access_token}";
            return await oAuthClient.PostJsonAsync(strApiUrl, tagMessage);
        }

        /// <summary>
        /// <![CDATA[根据openid，群发消息]]>
        /// </summary>
        /// <param name="oAuthClient"></param>
        /// <param name="access_token"></param>
        /// <param name="massMessage"></param>
        /// <returns><![CDATA[{"errcode":0,"errmsg":"send job submission success","msg_id":34182,"msg_data_id":206227730}]]></returns>
        public static async Task<string> SendAll(this OAuth.IOAuthClient oAuthClient, string access_token, Domain.Mass.MassMessageBase massMessage)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={access_token}";
            return await oAuthClient.PostJsonAsync(strApiUrl, massMessage);
        }
    }
}
