﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WeiXin.Sdk.Common.Extensions
{
    /// <summary>
    /// <![CDATA[用户标签]]>
    /// </summary>
    public static class TagClientExtentions
    {
        /// <summary>
        /// <![CDATA[批量打标签]]>
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static async Task<string> BatchTagging(this OAuth.IOAuthClient oAuthClient, string access_token, Domain.Tagging.Tag tag)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token={access_token}";
            return await oAuthClient.PostJsonAsync(requestUri: strApiUrl, data: tag);
        }


        /// <summary>
        /// <![CDATA[获取标签下粉丝列表]]>
        /// </summary>
        /// <param name="oAuthClient"></param>
        /// <param name="access_token"></param>
        /// <param name="data"><![CDATA[{   "tagid" : 134,   "next_openid":""//第一个拉取的OPENID，不填默认从头开始拉取 }]]></param>
        /// <returns><![CDATA[
        /// {   "count":2,//这次获取的粉丝数量   
        ///"data":{//粉丝列表
        ///"openid":[
        ///"ocYxcuAEy30bX0NXmGn4ypqx3tI0",
        ///"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"  ]},  
        ///"next_openid":"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"//拉取列表最后一个用户的openid }
        /// ]]></returns>
        public static async Task<string> Get(this OAuth.IOAuthClient oAuthClient, string access_token, object data)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token={access_token}";
            return await oAuthClient.PostJsonAsync(requestUri: strApiUrl, data: data);
        }


        /// <summary>
        /// <![CDATA[获取已创建的标签]]>
        /// </summary>
        /// <param name="oAuthClient"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        public static async Task<string> GetTags(this OAuth.IOAuthClient oAuthClient, string access_token)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/tags/get?access_token={access_token}";
            return await oAuthClient.GetAsync(requestUri: strApiUrl);
        }


        /// <summary>
        /// <![CDATA[获取用户身上的标签]]>
        /// </summary>
        /// <param name="oAuthClient"></param>
        /// <param name="access_token"></param>
        /// <param name="openId"></param>
        /// <returns><![CDATA[{"tagid_list":[//被置上的标签列表 134, 2] }]]></returns>
        public static async Task<string> GetUserTagList(this OAuth.IOAuthClient oAuthClient, string access_token, string openId)
        {
            string strApiUrl = $"https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token={access_token}";
            return await oAuthClient.PostJsonAsync(requestUri: strApiUrl, data: new { openid = openId });
        }
    }
}
