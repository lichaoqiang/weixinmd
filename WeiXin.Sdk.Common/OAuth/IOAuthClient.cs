﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WeiXin.Sdk.Common.OAuth
{

    /// <summary>
    /// 
    /// </summary>
    public interface IOAuthClient
    {

        /// <summary>
        /// <![CDATA[获取访问令牌]]>
        /// </summary>
        /// <param name="grant_type"></param>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        Task<string> GetAccessTokenAsync(string grant_type, string appid, string secret);

        /// <summary>
        /// <![CDATA[上传素材]]>
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        Task<string> UploadAsync(string access_token, string filename, string type, Stream stream);

        /// <summary>
        /// <![CDATA[获取用户信息]]>
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="openid"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        Task<string> GetUserInfoAsync(string access_token, string openid, string lang = "zh_CN");


        /// <summary>
        /// <![CDATA[POST一个JSON数据]]>
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<string> PostJsonAsync(string requestUri, object data);

        /// <summary>
        /// <![CDATA[GET请求]]>
        /// </summary>
        /// <param name="requestUri"></param>
        /// <returns></returns>
        Task<string> GetAsync(string requestUri);
    }
}
