﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Mass
{
    /// <summary>
    /// <![CDATA[批量消息基类]]>
    /// </summary>
    public abstract class MassMessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public List<string> touser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string msgtype { get; protected set; }
    }
}
