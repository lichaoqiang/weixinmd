﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Mass
{
    /// <summary>
    /// <![CDATA[文字消息]]>
    /// </summary>
    public class TextMessage : MassMessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public TextMessage() { msgtype = "text"; }

        /// <summary>
        /// 内容
        /// </summary>
        public ValueObject.Text text { get; set; }
    }
}
