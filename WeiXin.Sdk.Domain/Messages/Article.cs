﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WeiXin.Sdk.Domain.Messages
{
    /// <summary>
    /// 
    /// </summary>
    [XmlType("item")]
    public class Article
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string PicUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }
}
