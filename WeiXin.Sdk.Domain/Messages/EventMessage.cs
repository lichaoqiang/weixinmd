﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{
    /// <summary>
    /// <![CDATA[事件消息]]>
    /// </summary>
    public class EventMessage : MessageBase
    {

        /// <summary>
        /// 
        /// </summary>
        public EventMessage()
        {
            MsgType = "event";
        }

        /// <summary>
        /// 事件类型（CLICK,VIEW,subscribe）
        /// </summary>
        public string Event { get; set; }

        /// <summary>
        /// 事件KEY值，与自定义菜单接口中KEY值对应
        /// </summary>
        public string EventKey { get; set; }
    }
}
