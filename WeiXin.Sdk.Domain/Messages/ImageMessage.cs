﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{
    /// <summary>
    /// <![CDATA[图片消息]]>
    /// </summary>
    public class ImageMessage : MessageBase
    {


        /// <summary>
        /// 
        /// </summary>
        public ImageMessage()
        {
            this.MsgType = "image";
        }

        /// <summary>
        /// 
        /// </summary>
        public Image Image { get; set; }
    }
}
