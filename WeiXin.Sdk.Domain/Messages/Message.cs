﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{

    /// <summary>
    /// <![CDATA[公共MESSAGE参数]]>
    /// </summary>
    public class Message : MessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Message()
        {

        }

        /// <summary>
        /// <![CDATA[事件]]>
        /// </summary>
        public string Event { get; set; }

        /// <summary>
        /// <![CDATA[KEY值]]>
        /// </summary>
        public string EventKey { get; set; }

        /// <summary>
        /// <![CDATA[文本消息]]>
        /// </summary>
        public string Content { get; set; }
    }
}
