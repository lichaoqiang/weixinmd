﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MessageBase
    {
        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName { get; set; }

        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName { get; set; }

        /// <summary>
        /// 	消息创建时间 （整型）
        /// </summary>
        public long CreateTime { get; set; }

        /// <summary>
        /// 	消息类型，event
        /// </summary>
        public virtual string MsgType { get; protected set; }


        /// <summary>
        /// 消息ID
        /// </summary>
        public long MsgId { get; set; }
    }
}
