﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{
    /// <summary>
    /// 图文消息
    /// </summary>
    public class TextImageMessage : MessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public TextImageMessage()
        {
            MsgType = "MsgType";
        }

        /// <summary>
        /// 图文消息个数，限制为8条以内
        /// </summary>
        public int ArticleCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Article> Articles { get; set; }

        /// <summary>
        /// 图文消息标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PicUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }
}
