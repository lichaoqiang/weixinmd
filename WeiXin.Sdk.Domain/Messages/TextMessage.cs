﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Messages
{

    /// <summary>
    /// <![CDATA[文本消息]]>
    /// </summary>
    public class TextMessage : MessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public TextMessage()
        {
            MsgType = "text";
        }

        /// <summary>
        /// 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
        /// </summary>
        public string Content { get; set; }
    }
}
