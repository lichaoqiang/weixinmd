﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Sender
{

    /// <summary>
    /// 
    /// </summary>
    public class Article
    {
        /// <summary>
        /// <![CDATA[标题]]>
        /// </summary>
        public string title { get; set; }

        /// <summary>
        ///<![CDATA[描述]]>
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// <![CDATA[URL]]>
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// <![CDATA[图片]]>
        /// </summary>
        public string picurl { get; set; }
    }
}
