﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Sender
{
    /// <summary>
    /// <![CDATA[图文消息]]>
    /// </summary>
    public class KFArtcileMessage : KFMessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public KFArtcileMessage()
        {
            msgtype = "news";
        }

        /// <summary>
        /// 
        /// </summary>
        public News news { get; set; }
    }
}
