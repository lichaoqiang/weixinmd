﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Sender
{
    /// <summary>
    /// <![CDATA[图片消息]]>
    /// </summary>
    public class KFImageMessage : KFMessageBase
    {

        /// <summary>
        /// 
        /// </summary>
        public KFImageMessage()
        {
            msgtype = "image";
        }

        /// <summary>
        /// <![CDATA[素材]]>
        /// </summary>
        public Media image { get; set; }
    }
}
