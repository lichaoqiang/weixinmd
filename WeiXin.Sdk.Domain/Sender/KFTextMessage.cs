﻿using System;
using System.Collections.Generic;
using System.Text;
using WeiXin.Sdk.Domain.ValueObject;

namespace WeiXin.Sdk.Domain.Sender
{
    /// <summary>
    /// <![CDATA[文本消息]]>
    /// </summary>
    public class KFTextMessage : KFMessageBase
    {

        /// <summary>
        /// 
        /// </summary>
        public KFTextMessage()
        {
            msgtype = "text";
        }

        /// <summary>
        /// <![CDATA[文本内容]]>
        /// </summary>
        public Text text { get; set; }
    }


}
