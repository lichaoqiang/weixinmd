﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Tagging
{

    /// <summary>
    /// <![CDATA[标签]]>
    /// </summary>
    public class Tag
    {
        /// <summary>
        /// <![CDATA[openids]]>
        /// </summary>
        public List<string> openid_list { get; set; }

        /// <summary>
        /// <![CDATA[标签ID]]>
        /// </summary>
        public int tagid { get; set; }
    }
}
