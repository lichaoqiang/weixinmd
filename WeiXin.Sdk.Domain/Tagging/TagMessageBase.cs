﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeiXin.Sdk.Domain.Tagging
{
    /// <summary>
    /// <![CDATA[根据标签推送消息]]>
    /// </summary>
    public abstract class TagMessageBase
    {

        /// <summary>
        /// 
        /// </summary>
        public Filter filter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string msgtype { get; protected set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class Filter
    {
        /// <summary>
        /// 
        /// </summary>
        public bool is_to_all { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int tag_id { get; set; }
    }
}
