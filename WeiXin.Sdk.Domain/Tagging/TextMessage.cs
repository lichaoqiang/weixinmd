﻿using System;
using System.Collections.Generic;
using System.Text;
using WeiXin.Sdk.Domain.ValueObject;

namespace WeiXin.Sdk.Domain.Tagging
{
    /// <summary>
    /// <![CDATA[文本消息]]>
    /// </summary>
    public class TextMessage : TagMessageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public TextMessage() { msgtype = "text"; }

        /// <summary>
        /// <![CDATA[文本内容]]>
        /// </summary>
        public Text text { get; set; }
    }

}
