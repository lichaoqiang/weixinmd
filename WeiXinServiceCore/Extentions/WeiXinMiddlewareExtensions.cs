﻿using Microsoft.AspNetCore.Builder;

namespace WeiXinServiceCore.Extentions
{
    /// <summary>
    /// <![CDATA[微信中间件扩展]]>
    /// </summary>
    public static class WeiXinMiddlewareExtensions
    {

        /// <summary>
        /// <![CDATA[微信中间件]]>
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serverOptions"></param>
        public static void UseWeiXinServer(this IApplicationBuilder app, OAuth.WeiXinServerOptions serverOptions)
        {
            app.UseMiddleware<Middleware.WeiXinMiddleware>(serverOptions);
        }
    }
}
