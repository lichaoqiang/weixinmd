﻿using Microsoft.Extensions.DependencyInjection;

namespace WeiXinServiceCore.Extentions
{
    /// <summary>
    /// <![CDATA[微信服务扩展类]]>
    /// </summary>
    public static class WeiXinServiceCollectionExtensions
    {
        /// <summary>
        /// <![CDATA[添加微信服务]]>
        /// </summary>
        /// <param name="services"></param>
        public static void AddWeiXinServer(this IServiceCollection services)
        {
            services.AddSingleton(typeof(OAuth.IWeiXinServerProvider), typeof(OAuth.WeiXinServer));//单例：IOC注册服务类型
        }
    }
}