﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WeiXinServiceCore.OAuth;

namespace WeiXinServiceCore.Implement
{

    /// <summary>
    /// <![CDATA[考拉微信服务]]>
    /// </summary>
    public class KaoLaServer : OAuth.WeiXinServer, OAuth.IWeiXinServerProvider
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task OnRecieve(HttpContext context)
        {
            return base.OnRecieve(context);
        }


        /// <summary>
        /// <![CDATA[用户关注事件]]>
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task OnSubscribe(WeiXinContext context)
        {
            context.HttpContext.Response.StatusCode = 200;
            context.HttpContext.Response.ContentType = "text/plain";
            return context.HttpContext.Response.WriteAsync("lichaoqiang.com");
            return base.OnSubscribe(context);
        }


        /// <summary>
        /// <![CDATA[用户扫描事件]]>
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task OnScan(WeiXinContext context)
        {
            return base.OnScan(context);
        }
    }
}
