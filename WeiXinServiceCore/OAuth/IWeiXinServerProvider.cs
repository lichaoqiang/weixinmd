﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeiXinServiceCore.OAuth
{
    /// <summary>
    /// <![CDATA[微信服务提供者]]>
    /// </summary>
    public interface IWeiXinServerProvider
    {

        /// <summary>
        /// 21+1你+1丁-3-6=14
        /// </summary>
        OAuth.WeiXinServerOptions ServerOptions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="configuration"></param>
        /// <param name="serverOptions"></param>
        /// <returns></returns>
        Task Run(HttpContext context, IConfiguration configuration);
    }
}
