﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeiXinServiceCore.OAuth
{

    /// <summary>
    /// <![CDATA[配置选项]]>
    /// </summary>
    public class WeiXinServerOptions
    {
        /// <summary>
        ///<![CDATA[微信通知地址]]>
        /// </summary>
        public PathString NotifyPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private IWeiXinServerProvider _ServerProvider = null;

        /// <summary>
        /// <![CDATA[微信服务提供程序]]>
        /// </summary>
        public IWeiXinServerProvider WeiXinServerProvider
        {
            get
            {
                return _ServerProvider;
            }
            set
            {
                _ServerProvider = value;
                _ServerProvider.ServerOptions = this;
            }
        }

        /// <summary>
        /// <![CDATA[当接收到消息时]]>
        /// </summary>
        public Func<HttpContext, Task> OnRecieveAsync { get; set; }

        /// <summary>
        /// <![CDATA[扫描事件]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnScanAsync { get; set; }

        /// <summary>
        /// <![CDATA[关注事件]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnSubscribeAsync { get; set; }

        /// <summary>
        /// <![CDATA[取消关注]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnUnsubscribeAsync { get; set; }

        /// <summary>
        /// <![CDATA[菜单点击事件]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnClickAsync { get; set; }

        /// <summary>
        /// <![CDATA[点击链接]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnViewAsync { get; set; }

        /// <summary>
        /// <![CDATA[上报地理位置]]>
        /// </summary>
        public Func<WeiXinContext, Task> OnLocationAsync { get; set; }

        /// <summary>
        /// <![CDATA[被动接收普通消息]]>
        /// </summary>
        public Func<HttpContext, Task> OnRecieveMessageAsync { get; set; }
    }
}
