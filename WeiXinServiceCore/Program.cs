﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using log4net.Repository;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace WeiXinServiceCore
{

    /// <summary>
    /// 
    /// </summary>
    public class Program
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            string strRootPath = AppDomain.CurrentDomain.BaseDirectory;
            ILoggerRepository repository = LogManager.CreateRepository("DTKRepository");
            FileInfo file = new FileInfo(System.IO.Path.Combine(strRootPath, "Config/logger.config"));
            log4net.Config.XmlConfigurator.Configure(repository, file);//配置日志
            CreateWebHostBuilder(args).Build().Run();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                          .UseKestrel()
                          .UseContentRoot(Directory.GetCurrentDirectory())
                          .UseWebRoot(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
                          .UseUrls("http://localhost:5000")
                          .UseStartup<Startup>();
        }
    }
}
