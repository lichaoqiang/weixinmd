﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WeiXinServiceCore.Extentions;

namespace WeiXinServiceCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddWeiXinServer();//IOC注册服务类型
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //使用微信中间件
            app.UseWeiXinServer(new OAuth.WeiXinServerOptions()
            {
                NotifyPath = new PathString("/OAuth/WeiXin"),
                WeiXinServerProvider = new Implement.KaoLaServer(),
                OnScanAsync = (context) => { return Task.Delay(0); },
                OnClickAsync = (context) => { return Task.Delay(0); },
                OnSubscribeAsync = (context) => { return Task.Delay(0); },
                OnUnsubscribeAsync = (context) => { return Task.Delay(0); },
                OnViewAsync = (context) => { return Task.Delay(0); },
                OnRecieveMessageAsync = (context) => { return Task.Delay(0); },
            });

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
